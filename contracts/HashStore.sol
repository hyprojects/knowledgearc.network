pragma solidity ^0.5.8;

contract HashStore {
    mapping(address => string) internal ownerToIPFSHash;

    function save(string memory hash) public {
      ownerToIPFSHash[msg.sender] = hash;
    }

    function get() public view returns (string memory) {
        return ownerToIPFSHash[msg.sender];
    }
}
