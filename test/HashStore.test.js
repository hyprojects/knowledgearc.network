var HashStore = artifacts.require("./HashStore.sol");

contract('HashStore', function(accounts) {
    it("saves a hash", async function() {
        const owner = accounts[0]
        const hash = "QmQnBuMf4xKFvv3WuTCQVS5JnSnRz3qCijE3tT7fZtrWe8"

        const hashStore = await HashStore.deployed()

        await hashStore.save(hash, {from: owner})

        assert.equal(await hashStore.get({from: owner}), hash)
    })
});
