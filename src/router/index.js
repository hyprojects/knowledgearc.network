import Vue from 'vue'
import Router from 'vue-router'
import Repository from '@/components/Repository'
import HashStore from '@/components/HashStore'
import Settings from '@/components/Settings'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Repository',
            component: Repository,
        },
        {
            path: '/add',
            name: 'HashStore',
            component: HashStore
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Settings
        }
    ]
})
